const monthly = document.querySelectorAll(".monthly");
const anually = document.querySelectorAll(".annually");
const switchButton = document.querySelector(".switch-button");
const buttonContainer = document.querySelector(".button-container");

window.onload = () => {
    buttonContainer.addEventListener("click", () => {
        switchButton.classList.toggle("sb-active");
        anually.forEach((e) => e.classList.toggle("annually-active"));
        monthly.forEach((e) => e.classList.toggle("monthly-inactive"));
    });
};
